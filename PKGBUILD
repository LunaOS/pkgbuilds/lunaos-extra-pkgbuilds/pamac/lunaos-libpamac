_pkgname=libpamac
pkgname=$_pkgname-lunaos
pkgver=11.7.2
_sover=11.7
pkgrel=4
pkgdesc="Library for Pamac package manager based on libalpm"
arch=('any')
url="https://github.com/manjaro/libpamac/"
license=('GPL-3.0-or-later')
_commit=a6761a47a34011c5eaf050b6470f21722454a3e7
depends=(
  'appstream'
  'dbus-glib'
  'git'
  'glib2'
  'json-glib'
  'libalpm.so=15'
  'libsoup3'
  'flatpak'
  'archlinux-appstream-data'
  'libnotify'
  'polkit'
)
makedepends=(
  'appstream'
  'asciidoc'
  'flatpak'
  'git'
  'gobject-introspection'
  'meson'
  'vala'
  'systemd'
)


backup=('etc/pamac.conf')
provides=("libpamac.so=${_sover}" "libpamac-appstream.so=${_sover}" "libpamac-aur.so=${_sover}" "libpamac-flatpak.so=${_sover}" "libpamac=$pkgver")
source=("libpamac-$pkgver-$pkgrel.zip::$url/archive/$_commit.zip"
        "lunaos-default-settings.patch")
sha256sums=('ece7b15873f8b2282f7126938c5cf04415d34455e8a5c3f4306288f03e325d89'
            '7893a6a013c22cb314eee309f38e818f614cbcfab589cb9e75f6b04d7f430d87')
install=$_pkgname.install

create_links() {
  # create soname links
  find "$pkgdir" -type f -name '*.so*' ! -path '*xorg/*' -print0 | while read -d $'\0' _lib; do
      _soname=$(dirname "${_lib}")/$(readelf -d "${_lib}" | grep -Po 'SONAME.*: \[\K[^]]*' || true)
      _base=$(echo ${_soname} | sed -r 's/(.*)\.so.*/\1.so/')
      [[ -e "${_soname}" ]] || ln -s $(basename "${_lib}") "${_soname}"
      [[ -e "${_base}" ]] || ln -s $(basename "${_soname}") "${_base}"
  done
}

prepare() {
  # adjust version string
  sed -i -e "s|\"$pkgver\"|\"$pkgver-$pkgrel\"|g" libpamac-$_commit/src/version.vala
  patch -d "libpamac-$_commit" -p1 -i "${srcdir}"/lunaos-default-settings.patch
}


build() {
	arch-meson "libpamac-$_commit" 'build' -Denable-snap=false -Denable-flatpak=true -Denable-appstream=true
	meson compile -C 'build'
}

package() {
	meson install -C 'build' --no-rebuild --destdir="$pkgdir"
	install -Dm644 "libpamac-$_commit/COPYING" "${pkgdir}/usr/share/licenses/${_pkgname}/LICENSE"
	install -Dm644 "build/src/libpamac-aur.so.${_sover}" "$pkgdir/usr/lib/libpamac-aur.so.${_sover}"
	install -Dm644 "build/src/libpamac-appstream.so.${_sover}" "$pkgdir/usr/lib/libpamac-appstream.so.${_sover}"
	install -Dm644 "build/src/libpamac-flatpak.so.${_sover}" "$pkgdir/usr/lib/libpamac-flatpak.so.${_sover}"
	create_links
}

